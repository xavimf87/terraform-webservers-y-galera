// Web servers

resource "hcloud_server" "web" {
  count       = var.instances_frontend
  name        = "web-server-${count.index}"
  image       = var.os_type
  server_type = var.server_type
  location    = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
  labels = {
    type = "web"
  }
  user_data = file("web_servers.yml")

}

// Modifiquem el wp-config.php de wordpress

resource "null_resource" "web" {
  count = length(hcloud_server.web)

  connection {
    host = "${element(hcloud_server.web.*.ipv4_address, count.index)}"
  }
  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "sed -i 's,database_name_here,${var.wordpress_database},' /var/www/wordpress/wp-config.php",
      "sed -i 's,username_here,${var.wordpress_db_user},' /var/www/wordpress/wp-config.php",
      "sed -i 's,password_here,${var.wordpress_db_password},' /var/www/wordpress/wp-config.php",
      "sed -i 's,localhost,${hcloud_load_balancer.backend_lb.ipv4},' /var/www/wordpress/wp-config.php",
      "touch modificado-wordpress.txt"
    ]
  }
}


// Sensu: Instal·lar agent i plugins.
resource "null_resource" "web_sensu" {
  count = length(hcloud_server.web)

  connection {
    host = "${element(hcloud_server.web.*.ipv4_address, count.index)}"
  }
  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "curl -s https://packagecloud.io/install/repositories/sensu/stable/script.deb.sh | bash",
      "curl -s https://packagecloud.io/install/repositories/sensu/community/script.deb.sh | bash",
      "sed -i 's/debian/ubuntu/g' /etc/apt/sources.list.d/sensu_community.list",
      "sed -i 's/buster/bionic/g' /etc/apt/sources.list.d/sensu_community.list",
      "apt update",
      "apt -y install build-essential ruby ruby-dev sensu-go-agent sensu-plugins-ruby",
      "/opt/sensu-plugins-ruby/embedded/bin/gem install vmstat",
      "sensu-install -P sensu-plugins-disk-checks,sensu-plugins-cpu-checks,sensu-plugins-network-checks,sensu-plugins-memory-checks,sensu-plugins-load-checks",
      "echo \"sensu   ALL=(ALL:ALL) NOPASSWD: ALL\" >>  /etc/sudoers.d/sensu && chmod 0440 /etc/sudoers.d/sensu && chown root.root /etc/sudoers.d/sensu",
      "apt-get install sysstat -y",
      "apt install default-libmysqlclient-dev -y",
      // Sensu plugins
      "sensu-install -P sensu-plugins-nginx",
      "sensu-install -P sensu-plugins-io-checks",
      // Sensu agent
      "mkdir -p /etc/sensu",
      "touch /etc/sensu/agent.yml",
      "echo \"---\" >> /etc/sensu/agent.yml",
      "echo 'name: \"${element(hcloud_server.web.*.name, count.index)}\"' >> /etc/sensu/agent.yml",
      "echo \"subscriptions:\" >> /etc/sensu/agent.yml",
      "echo \"  - deploy:\" >> /etc/sensu/agent.yml",
      "echo \"  - system:\" >> /etc/sensu/agent.yml",
      "echo \"  - metrics-base:\" >> /etc/sensu/agent.yml",
      "echo \"  - nginx:\" >> /etc/sensu/agent.yml",
      "echo \"backend-url:\" >> /etc/sensu/agent.yml",
      "echo '  - \"wss://${var.sensu_domain}:${var.sensu_port}\"' >> /etc/sensu/agent.yml",
      "systemctl enable --now sensu-agent",
      "touch sensu-y-sensu-plugins-instalados"
    ]
  }
}

