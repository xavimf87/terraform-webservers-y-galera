resource "hcloud_load_balancer" "backend_lb" {
  name               = "backend_lb"
  load_balancer_type = "lb11"
  location           = var.location
  labels = {
    type = "backend"
  }

  dynamic "target" {
    for_each = hcloud_server.galera
    content {
      type      = "server"
      server_id = target.value["id"]
    }
  }
  
  algorithm {
    type = "round_robin"
  }
}

resource "hcloud_load_balancer_service" "backend_lb_service" {
  load_balancer_id = hcloud_load_balancer.backend_lb.id
  protocol         = var.galera_protocol
  listen_port      = var.galera_port
  destination_port = var.galera_port

}

resource "hcloud_load_balancer_network" "galera_network" {
  load_balancer_id        = hcloud_load_balancer.backend_lb.id
  subnet_id               = hcloud_network_subnet.hc_private_subnet.id
  enable_public_interface = "true"
}
