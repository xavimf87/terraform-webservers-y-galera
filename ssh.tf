resource "hcloud_ssh_key" "default" {
  name       = "terraform"
  public_key = file("~/.ssh/terraform.pub")
}