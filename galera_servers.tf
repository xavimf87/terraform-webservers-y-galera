

resource "hcloud_server" "galera" {
  count       = var.instances_backend
  name        = "galera-server-${count.index}"
  image       = var.os_type
  server_type = var.server_type
  location    = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
  labels = {
    type = "backend"
  }
  user_data = file("galera_servers.yml")

  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "sed -i '/^\\[mysqld\\]/a\\wsrep_node_address=${self.ipv4_address}' /etc/mysql/mariadb.conf.d/50-server.cnf",
      "sed -i '/^\\[mysqld\\]/a\\wsrep_node_name=${self.name}' /etc/mysql/mariadb.conf.d/50-server.cnf",
      // Limit no file
      "sed -i 's,LimitNOFILE=16384,LimitNOFILE=64000,' /etc/systemd/system/mysql.service",
      
      "systemctl daemon-reload"
    ]
    connection {
      type        = "ssh"
      user        = "root"
      host = self.ipv4_address
      private_key = file("~/.ssh/terraform")
    }
  }
}



// gcom
resource "null_resource" "galera" {
  count = length(hcloud_server.galera)

  connection {
    host = "${element(hcloud_server.galera.*.ipv4_address, count.index)}"
  }
  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "sed -i '/^\\[mysqld\\]/a\\wsrep_cluster_address=\"gcomm://${join(",", hcloud_server.galera.*.ipv4_address)}\"' /etc/mysql/mariadb.conf.d/50-server.cnf",
      "touch creado_gcomm"
    ]
  }
}
// Start galera
resource "null_resource" "galera0" {

  connection {
    host = "${element(hcloud_server.galera.*.ipv4_address, 0)}"
  }
  provisioner "remote-exec" {
    inline = [
      "galera_new_cluster"
    ]
  }
}

// Sensu: Instal·lar agent i plugins.
resource "null_resource" "galera_sensu" {
  count = length(hcloud_server.galera)

  connection {
    host = "${element(hcloud_server.galera.*.ipv4_address, count.index)}"
  }
  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "curl -s https://packagecloud.io/install/repositories/sensu/stable/script.deb.sh | bash",
      "curl -s https://packagecloud.io/install/repositories/sensu/community/script.deb.sh | bash",
      "sed -i 's/debian/ubuntu/g' /etc/apt/sources.list.d/sensu_community.list",
      "sed -i 's/buster/bionic/g' /etc/apt/sources.list.d/sensu_community.list",
      "apt update",
      "apt -y install build-essential ruby ruby-dev sensu-go-agent sensu-plugins-ruby",
      "/opt/sensu-plugins-ruby/embedded/bin/gem install vmstat",
      "sensu-install -P sensu-plugins-disk-checks,sensu-plugins-cpu-checks,sensu-plugins-network-checks,sensu-plugins-memory-checks,sensu-plugins-load-checks",
      "echo \"sensu   ALL=(ALL:ALL) NOPASSWD: ALL\" >>  /etc/sudoers.d/sensu && chmod 0440 /etc/sudoers.d/sensu && chown root.root /etc/sudoers.d/sensu",
      "apt-get install sysstat -y",
      "apt install default-libmysqlclient-dev -y",
      // Sensu plugins
      "sensu-install -P sensu-plugins-mysql",
      "sensu-install -P sensu-plugins-io-checks",
      // Sensu agent
      "mkdir -p /etc/sensu",
      "touch /etc/sensu/agent.yml",
      "echo \"---\" >> /etc/sensu/agent.yml",
      "echo 'name: \"${element(hcloud_server.galera.*.name, count.index)}\"' >> /etc/sensu/agent.yml",
      "echo \"subscriptions:\" >> /etc/sensu/agent.yml",
      "echo \"  - deploy:\" >> /etc/sensu/agent.yml",
      "echo \"  - system:\" >> /etc/sensu/agent.yml",
      "echo \"  - metrics-base:\" >> /etc/sensu/agent.yml",
      "echo \"  - mysql:\" >> /etc/sensu/agent.yml",
      "echo \"backend-url:\" >> /etc/sensu/agent.yml",
      "echo '  - \"wss://${var.sensu_domain}:${var.sensu_port}\"' >> /etc/sensu/agent.yml",
      "systemctl enable --now sensu-agent",
      "touch sensu-y-sensu-plugins-instalados"
    ]
  }
}

// Creació del usuari Wordpress

resource "null_resource" "galera-wordpress" {
  count = length(hcloud_server.galera)

  connection {
    host = "${element(hcloud_server.galera.*.ipv4_address, count.index)}"
  }
  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "systemctl start mysql",
      "mysql -u root -e \"CREATE DATABASE ${var.wordpress_database}; grant all privileges on ${var.wordpress_database}.* to '${var.wordpress_db_user}'@'%' identified by '${var.wordpress_db_password}';flush privileges;\"",
      "touch creado_user_wordpress"
    ]
  }
}

// Creacio del usuari sensu mysql
resource "null_resource" "galera_sensu_user" {
 count = length(hcloud_server.galera)

  connection {
    host = "${element(hcloud_server.galera.*.ipv4_address, count.index)}"
  }
  provisioner "remote-exec" {
    inline = [
      "mysql -u root -e \"grant all privileges on *.* to '${var.sensu_mysql_user}'@'127.0.0.1' identified by '${var.sensu_mysql_pass}';flush privileges;\"",
      "touch creado_user_mysql"
    ]
  
  }
}
