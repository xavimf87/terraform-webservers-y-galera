variable "hcloud_token" {
  default = ""
}

variable "instances_frontend" {
  default = "2"
}
variable "instances_backend" {
  default = "3"
}

variable "location" {
  default = "nbg1"
}

variable "server_type" {
  default = "cx11"
}

variable "os_type" {
  default = "debian-10"
}

variable "disk_size" {
  default = "20"
} 

variable "ip_range" {
  default = "10.0.1.0/24"
}


variable "http_protocol" {
  default = "http"
}

variable "http_port" {
  default = "80"
}

variable "galera_protocol" {
  default = "tcp"
}

variable "galera_port" {
  default = "3306"
}

variable "wordpress_db_user" {
  default = "wordpress"
}

variable "wordpress_db_password" {
  default = "vahKei4ovah0ieLasohaceiTahxeelae"
}

variable "wordpress_database" {
  default = "wordpress"
  
}

/*
  Sensu
*/
variable "sensu_domain" {
  default = "agent.devops-alumno09.com"
}
variable "sensu_port" {
  default = "18082"
}

variable "sensu_mysql_user" {
  default = "sensu"
}
variable "sensu_mysql_pass" {
  default = "unpasswordmuyseguro"
}


